# Example of using a service

Position yourself in this directory first.

### Step 1

Build a docker image of a service:
```bash
cd ..
docker build -t screenshotasaservice .
cd -
```

### Step 2

Create a directory where data will be stored
```bash
mkdir storage
```

### Step 3

Run service and fileserver:
```bash
docker run -it -p 8000:8000 -v $PWD'/storage/':/storage/ -e HOSTNAME="localhost:80/" -d screenshotasaservice

docker run -it -p 80:80 -v $PWD'/storage/':/usr/share/nginx/html/ -d nginx
```

### Step 4

Create a file with URLs
```bash
touch urls.txt
```

Put some URLs in file
```txt
http://google.com
https://jsoneditoronline.org
https://www.etf.bg.ac.rs
```

### Step 5

Run the script and see the output:
```bash
pipenv install
pipenv shell
./generate_screenshots urls.txt
```

### Step 6

Try to access the output from a browser.