# Screenshot as a Service

Implementation of backend service for capturing screenshots.

## Development

The recommended way of developing this project is to pull Docker image from registry or create one locally. Run it with mounting source files as a volume so all the changes to the source files will be immediately visible and the server will be reran.

Building Docker image:
```bash
docker build -t registry.gitlab.com/ivankulezic/screenshotasaservice:<tag> .
```

Running Docker image for develoment purposes:
```bash
docker run -it -p 8000:8000 -v <path-to-src-files>:<path-to-worskapce-inside-container> -v <path-to-storage-location>:/storage/ -e HOSTNAME=<fileserver-hostname> registry.gitlab.com/ivankulezic/screenshotasaservice:<tag>
```

## Overall architecture:

One instance of server has an endpoint to generate screenshots of URLs provided in the request body. Example of request:
```json
{
  "urls": [
    "http://google.com/",
    "https://jsoneditoronline.org/",
    "https://www.etf.bg.ac.rs"
  ]
}
```

Example of a response:
```json
{
"http://google.com/": "www.ssas.com/059d4cb7d2160037a141631b85b9e7f08c69c243550443e1521d2256086a047b.png",
"https://jsoneditoronline.org/": "www.ssas.com/0a1213f4f186971b38a6f573d1be389ab69b98a0c50fd590886844b8d0ff0566.png",
"https://www.etf.bg.ac.rs": "www.ssas.com/bad5891ccc81afb990e6083e658f1b1489011c8c3f79eec9f62d7199a802e2a7.png"
}
```

One instance of a fileserver serves results which can be fetched from URLs provided in response. 

They both share same data storage which is mounted to both containers as a volume.

![alt text](/docs/ArchitectureDiagram.png)

## Deployment

Run service as separate container:
```bash
docker run -it -p 8000:8000 -v <path-to-storage-location>:/storage/ -e HOSTNAME=<fileserver-hostname> registry.gitlab.com/ivankulezic/screenshotasaservice:<tag>
```

Run fileserver as a separate container:
```bash
docker run -it -p 80:80 -v <path-to-storage-location>:/usr/share/nginx/html/ nginx
```

## Scaling as an Enterprise Component

As a storage one of the cloud providers could be used, ideally with some kind of distributed storage system.

Service has a parameter `--workers` and it is suggested to pass it `2 * n + 1` where `n` is number of CPU cores. There can be many instances of this service running separately and storing generated screenshots to the same storage mentioned above(that is why I chose to deploy the service in a Docker container). All instances should again run on some cloud provider, where one can add more CPUs easily if necessary. Also it would be good to use Kubernetes to manage number of instances(pods) so in case one of them is shut down a new one will automatically run. This will allow to keep the same number of instances at all times. In front of these instances one nginx server can be used to do load balancing of incoming requests so that instances who are free or have less requests queued will be given more requests.

For the result serving there should be a couple of fileservers (again nginx can be used) and one nginx as a balancer in front of them, similar to the balancing described in the previous paragraph.

## Improvements

Improve CI job to build and push image to registry when there is a tag pushed on GitLab. Also add automatic deployment from CI job.
