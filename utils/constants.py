"""
Declares all the necessary constants used throught the server.
"""
IMAGE_EXTENSION = '.png'
STORAGE_LOCATION = '/storage/'
SCREENSHOT_WIDTH = 1280
SCREENSHOT_HEIGHT = 1024
ENCODING = 'utf-8'
