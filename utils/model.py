"""
Defines models used for requests and responses.
"""

from typing import List
from pydantic import BaseModel

class URLList(BaseModel): # pylint: disable=too-few-public-methods
    """
    Request model providing list of URLs to take a screenshot of.
    """
    urls: List[str]
