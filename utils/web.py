"""
Defines functions for generating screenshot based on a URL.
"""
import string
import hashlib

from random import choice
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import InvalidArgumentException
from pyvirtualdisplay import Display
from utils.constants import SCREENSHOT_WIDTH, SCREENSHOT_HEIGHT
from utils.constants import ENCODING, IMAGE_EXTENSION, STORAGE_LOCATION

def take_screenshot(url, host_location):
    """
    Generates screenshot of provided URL and returns its location.

    Args:
        url: URL to get the screenshots of.
        host_location: URL where the screenshot will be served.

    Returns:
        Location of the screenshot.
    """
    display = Display(visible=0, size=(SCREENSHOT_WIDTH, SCREENSHOT_HEIGHT))
    display.start()
    options = Options()
    options.headless = True
    driver = webdriver.Firefox(options=options)
    try:
        driver.get(url)
    except InvalidArgumentException:
        return "Invalid URL"
    image_name = hashlib.sha256((url + get_salt()).encode(ENCODING)).hexdigest() + IMAGE_EXTENSION
    driver.save_screenshot(STORAGE_LOCATION + image_name)
    driver.close()
    driver.quit()
    display.stop()
    return host_location + image_name

def get_salt():
    """
    Generates random 8 chars of salt.

    Returns:
        String containing radnom letters and digits.
    """
    return ''.join(choice(string.ascii_letters + string.digits) for i in range(8))
