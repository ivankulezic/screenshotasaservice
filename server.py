""" Defines available endpoints for this service.
"""
import json
import os

from fastapi import FastAPI
from utils.web import take_screenshot
from utils.model import URLList

APP = FastAPI()
BASE_URL = os.environ.get('HOSTNAME')

@APP.post("/fetchScreenshots")
def root(url_list: URLList):
    """
    Generates screenshots of provided URLs and returns their locations.

    Args:
        url_list: List of URLs to get the screenshots of.

    Returns:
        JSON containing mappings from received URLs to locations of generated screenshots.
    """
    response = dict()
    for url in url_list.urls:
        response[url] = take_screenshot(url, BASE_URL)

    return json.dumps(response)
