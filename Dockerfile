FROM python:3.7-alpine as build_deps

WORKDIR /app
COPY Pipfile Pipfile.lock /app/

RUN apk add --virtual=.build_deps gcc musl-dev make
RUN pip install --upgrade pip  && \
    pip install pipenv && \
    pipenv install --system && \
    wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz && \
    tar -xvzf geckodriver* && \
    chmod +x geckodriver && \
    mv geckodriver /usr/local/bin/

RUN apk del .build_deps

FROM python:3.7-alpine

COPY --from=build_deps /usr/local/lib/python3.7/site-packages/ /usr/local/lib/python3.7/site-packages/
COPY --from=build_deps /usr/local/bin/uvicorn /usr/local/bin/uvicorn
COPY --from=build_deps /usr/local/bin/geckodriver /usr/local/bin/geckodriver

RUN apk add dbus-x11 ttf-freefont firefox-esr xvfb

WORKDIR /app

COPY . /app

EXPOSE 8000

CMD uvicorn server:APP --workers 3 --host 0.0.0.0
